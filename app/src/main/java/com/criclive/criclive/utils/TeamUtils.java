package com.criclive.criclive.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rama on 21/08/16.
 */
public class TeamUtils {

    public static List<String> getInternationalList() {
        List<String> teamList = new ArrayList<>();
        teamList.add("All Matches");
        teamList.add("India");
        teamList.add("South Africa");
        teamList.add("Sri Lanka");
        teamList.add("Australia");
        teamList.add("England");
        teamList.add("Pakistan");
        teamList.add("West Indies");
        teamList.add("New Zealand");
        teamList.add("Bangladesh");
        teamList.add("Ireland");
        teamList.add("Kenya");
        teamList.add("Netherlands");
        teamList.add("Namibia");
        teamList.add("Afghanistan");
        return teamList;
    }

    public static List<String> getDomesticList() {
        List<String> teamList = new ArrayList<>();
        teamList.add("All Matches");
        teamList.add("Chennai Super Kings");
        teamList.add("Royal Challengers Bangalore");
        teamList.add("Delhi Daredevils");
        teamList.add("Deccan Chargers");
        teamList.add("Kings XI Punjab");
        teamList.add("Kolkata Knight Riders");
        teamList.add("Mumbai Indians");
        teamList.add("Pune Warriors");
        teamList.add("Rajasthan Royals");
        return teamList;
    }


    public static List<String> getSeriesNames() {
        List<String> teamList = new ArrayList<>();
        teamList.add("International");
        teamList.add("IPL T20");
        return teamList;
    }
}
