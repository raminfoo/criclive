package com.criclive.criclive.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.support.v4.util.LruCache;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by Rama on 21/08/16.
 */
public class AndroidUtils {

    private static Animation anim;

    public static void initAnim(Context mContext) {
        anim = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
        anim.setDuration(800);
    }

    public static Animation getAnim() {
        return anim;
    }


    public static void showToast(Context mContext, String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
        ;
    }


    public static void showToastLong(Context mContext, String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
        ;
    }


    public static boolean isConnected(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static ImageLoader getImageLoader(Context mContext) {
        ImageLoader mImageLoader = null;
        try {
            RequestQueue mRequestQueue = Volley.newRequestQueue(mContext);
            mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
                private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

                public void putBitmap(String url, Bitmap bitmap) {
                    mCache.put(url, bitmap);
                }

                public Bitmap getBitmap(String url) {
                    return mCache.get(url);
                }
            });
        } catch (Exception e) {
            return null;
        }
        return mImageLoader;
    }
}