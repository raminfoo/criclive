package com.criclive.criclive.utils;

import android.util.Xml;

import com.criclive.criclive.model.CricketModel;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rama on 21/08/16.
 */
public class XMLParser {

    public static List<CricketModel> parseData(String response) {
        // TODO Auto-generated method stub
        List<CricketModel> messages = null;
        XmlPullParser parser = Xml.newPullParser();
        try {
            parser.setInput(new StringReader(response));
            int eventType = parser.getEventType();
            CricketModel currentMessage = null;
            boolean done = false;
            while (eventType != XmlPullParser.END_DOCUMENT && !done) {
                String name = null;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        messages = new ArrayList<CricketModel>();
                        break;
                    case XmlPullParser.START_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("item")) {
                            currentMessage = new CricketModel();
                        } else if (currentMessage != null) {
                            if (name.equalsIgnoreCase("title")) {
                                currentMessage.setTitle(parser.nextText());
                            } else if (name.equalsIgnoreCase("description")) {
                                currentMessage.setDescription(parser.nextText());
                            } else if (name.equalsIgnoreCase("link")) {
                                currentMessage.setLink(parser.nextText());
                            } else if (name.equalsIgnoreCase("pubDate")) {
                                currentMessage.setPubDate(parser.nextText());
                            } else if (name.equalsIgnoreCase("image") || name.equalsIgnoreCase("largeimage")) {
                                currentMessage.setImage(parser.nextText());
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("item") && currentMessage != null) {
                            messages.add(currentMessage);
                        } else if (name.equalsIgnoreCase("channel")) {
                            done = true;
                        }
                        break;
                }
                eventType = parser.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return messages;
    }


}
