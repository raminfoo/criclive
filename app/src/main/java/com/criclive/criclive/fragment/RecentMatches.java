package com.criclive.criclive.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.criclive.criclive.R;
import com.criclive.criclive.adapter.CJRRecentUpcoming;
import com.criclive.criclive.model.CricketModel;
import com.criclive.criclive.utils.AndroidUtils;
import com.criclive.criclive.utils.XMLParser;
import com.criclive.criclive.volley.CustomVolleyRequestQueue;

import java.util.List;

/**
 * Created by Rama on 21/08/16.
 */
public class RecentMatches extends Fragment {

    private List<CricketModel> mListData;
    private ListView mListView;
    private TextView noMatch;
    private String url = "";
    private Context mContext;
    private RequestQueue mQueue;
    private ProgressBar mProgressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_live_match, container, false);
        mContext = getActivity().getApplicationContext();
        mListView = (ListView) view.findViewById(R.id.listView);
        noMatch = (TextView) view.findViewById(R.id.nomatch);
        mProgressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        Button btn_refresh = (Button)view. findViewById(R.id.btn_refresh);
        btn_refresh.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mProgressBar.setVisibility(View.VISIBLE);
                loadData();
            }
        });
        Bundle bundle = getArguments();
        url = bundle.getString("Url");

        loadData();
        return view;
    }

    private void loadData() {
        if (AndroidUtils.isConnected(mContext)) {
            mQueue = CustomVolleyRequestQueue.getInstance(mContext).getRequestQueue();
            StringRequest req = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            mListData = XMLParser.parseData(response);
                            if (mListData != null && mListData.size() > 0) {
                                updateUIwithData();
                            } else {
                                showErrorMessage("Please connect to the Internet to continue...");
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            showErrorMessage("Please connect to the Internet to continue...");
                        }
                    }
            );
            mQueue.add(req);
        } else {
            showErrorMessage("Please connect to the Internet to continue...");
        }
    }

    private void showErrorMessage(String message) {
        mListView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
        noMatch.setVisibility(View.VISIBLE);
        noMatch.setText(message);
    }


    public void updateUIwithData() {
        try {
            mListView.setAdapter(new CJRRecentUpcoming(mContext,mListData));
            mListView.setVisibility(View.VISIBLE);
            noMatch.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.GONE);
        } catch (Throwable t) {
            showErrorMessage("Please connect to the Internet to continue...");
        }
    }
}
