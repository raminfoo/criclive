package com.criclive.criclive.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListView;

import com.criclive.criclive.R;
import com.criclive.criclive.acitivity.AJRNewsListing;
import com.criclive.criclive.adapter.CJRTeamListing;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rama on 21/08/16.
 */
public class NewsList extends Fragment {


    private List<String> mTeamTitle = new ArrayList<String>();
    private List<String> mTeamPosition = new ArrayList<String>();
    private ListView mListView;
    private String[] mNewsList = new String[]{"Latest News", "India", "Australia", "Pakistan", "England",
            "South Africa", "Bangladesh", "New Zealand", "Sri Lanka", "West Indies", "Zimbabwe"};
    private String[] mPosition = new String[]{"0", "6", "2", "7", "1", "3", "25", "5", "8", "4", "9"};
    private Context mContext;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_team_list, container, false);
        mContext = getActivity().getApplicationContext();
        mListView = (ListView) view.findViewById(R.id.listView);

        for (int i = 0; i < mNewsList.length; i++) {
            mTeamTitle.add(mNewsList[i]);
            mTeamPosition.add(mPosition[i]);
        }

        mHandler.post(mUpdateResults);
        return view;
    }


    final Handler mHandler = new Handler();

    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            System.out.println("Inside runnable ...");
            updateUIData();
        }
    };

    public void updateUIData() {
        try {
            Animation anim = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            anim.setDuration(800);
            mListView.startAnimation(anim);
            mListView.setAdapter(new CJRTeamListing(mContext, mTeamTitle));
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                public void onItemClick(AdapterView<?> arg0, View arg1,
                                        int position, long arg3) {
                    Intent intent = new Intent(mContext, AJRNewsListing.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("selectedId", mTeamPosition.get(position));
                    bundle.putString("selectedTeam", mTeamTitle.get(position));
                    intent.putExtras(bundle);
                    startActivityForResult(intent, 100);

                }
            });
        } catch (Throwable t) {
            Log.e("AndroidNews", t.getMessage(), t);
        }
    }
}
