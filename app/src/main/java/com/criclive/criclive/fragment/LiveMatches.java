package com.criclive.criclive.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.criclive.criclive.R;
import com.criclive.criclive.acitivity.AJRAboutUs;
import com.criclive.criclive.acitivity.AJRWebPage;
import com.criclive.criclive.adapter.CJRLiveMatchesAdapter;
import com.criclive.criclive.model.CricketModel;
import com.criclive.criclive.utils.AndroidUtils;
import com.criclive.criclive.utils.XMLParser;
import com.criclive.criclive.volley.CustomVolleyRequestQueue;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Rama on 21/08/16.
 */
public class LiveMatches extends Fragment implements View.OnClickListener {

    ListView mListView;
    String feedUrlString = "http://static.espncricinfo.com/rss/livescores.xml";
    int totalSize = 0;
    TextView txtNoMatch;
    LinearLayout mNoInternetView;

    private Context mContext;
    private View view;
    private RequestQueue mQueue;
    private List<CricketModel> mCricketDataList;
    private HashMap<String, String> teamNameList;
    private Button mRefresh;
    private ProgressBar mProgressBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_live_match, container, false);
        mContext = getActivity().getApplicationContext();
        AndroidUtils.initAnim(mContext);
        initViews();
        loadData();
        return view;
    }

    private void initViews() {
        mListView = (ListView) view.findViewById(R.id.listView);
        txtNoMatch = (TextView) view.findViewById(R.id.nomatch);
        mNoInternetView = (LinearLayout) view.findViewById(R.id.noInternetView);
        mRefresh = (Button) view.findViewById(R.id.btn_refresh);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressBar.setVisibility(View.VISIBLE);
                mNoInternetView.setVisibility(View.GONE);
                loadData();
            }
        });
    }


    private void loadData() {
        if (AndroidUtils.isConnected(mContext)) {
            mQueue = CustomVolleyRequestQueue.getInstance(mContext).getRequestQueue();
            StringRequest req = new StringRequest(Request.Method.GET, feedUrlString,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            teamNameList = new HashMap<>();
                            mCricketDataList = XMLParser.parseData(response);
                            if (isValidate()) {
                                updateList();
                            } else {
                                showErrorMessage("No Match in progress..");
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            showErrorMessage("No Match in progress..");
                        }
                    }
            );
            mQueue.add(req);
        } else {
            showErrorMessage("Please connect to the Internet to continue...");
        }
    }

    private void addTeamNames() {
        int size = mCricketDataList.size();
        for (int i = 0; i < size; i++) {
            String[] teams = mCricketDataList.get(i).getTitle().split(" v ");
            if (teams[0].matches(".*\\d.*")) {
                String team1 = teams[0].split("[0-9]")[0];
                teamNameList.put(team1, team1);
            } else {
                teamNameList.put(teams[0], teams[0]);
            }

            if (teams[1].matches(".*\\d.*")) {
                String team2 = teams[1].split("[0-9]")[0];
                teamNameList.put(team2, team2);
            } else {
                teamNameList.put(teams[1], teams[1]);
            }
        }
    }


    private boolean isValidate() {
        Boolean isValid = true;
        try {
            int size = mCricketDataList.size();
            if (size > 0) {
                String title = mCricketDataList.get(0).getTitle();
                if (title.equalsIgnoreCase("No Match in progress..")) {
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isValid;
    }

    @Override
    public void onClick(View view) {

    }


    private void updateList() {
        try {
            totalSize = mCricketDataList.size();
            for (int i = 0; i < totalSize; i++) {
                String mTitle = mCricketDataList.get(i).getTitle();
                if (mTitle.contains(" v ") &&
                        mTitle.length() > 5) {
                    String[] splitTeams = mTitle.split(" v ");
                    String[] splitTeam1 = splitTeams[0].split(" [0-9]");
                    String[] splitTeam2 = splitTeams[1].split(" [0-9]");
                    mCricketDataList.get(i).setTeams(splitTeam1[0] + " vs " + splitTeam2[0]);
                    mCricketDataList.get(i).setTeam1(splitTeams[0]);
                    mCricketDataList.get(i).setTeam2(splitTeams[1]);
                }
            }

            updateUIwithData();

        } catch (Exception e) {
            showErrorMessage("Please connect to the Internet to continue...");
        }
    }

    private void showErrorMessage(String errorMessage) {
        mProgressBar.setVisibility(View.GONE);
        mListView.setVisibility(View.GONE);
        mNoInternetView.setVisibility(View.VISIBLE);
        txtNoMatch.setText(errorMessage);
    }

    public void updateUIwithData() {
        try {

            if (mCricketDataList.size() > 0) {
                mListView.setVisibility(View.VISIBLE);
                mNoInternetView.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.GONE);
                mListView.startAnimation(AndroidUtils.getAnim());
                mListView.setAdapter(new CJRLiveMatchesAdapter(mContext, mCricketDataList));
                mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> arg0, View arg1,
                                            int position, long arg3) {
                        Intent intent = new Intent(mContext, AJRWebPage.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("url", mCricketDataList.get(position).getLink());
                        bundle.putString("teamName", mCricketDataList.get(position).getTeams());
                        intent.putExtras(bundle);
                        startActivityForResult(intent, 100);
                    }
                });
            } else {
                showErrorMessage("No Match Available...");
            }

        } catch (Throwable t) {
            Log.e("AndroidNews", t.getMessage(), t);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, Menu.NONE, "Refresh").setTitle("Refresh");
        menu.add(0, 1, Menu.NONE, "AboutUs").setTitle("About Us");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0: {
                loadData();
            }
            return true;
            case 1: {
                Intent intent = new Intent(mContext, AJRAboutUs.class);
                startActivityForResult(intent, 100);
            }
            return true;
        }
        return false;
    }

}