package com.criclive.criclive.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.criclive.criclive.R;
import com.criclive.criclive.model.CricketModel;

import java.util.List;

public class CJRLiveMatchesAdapter extends BaseAdapter {

	LayoutInflater myInflater;
	List<CricketModel> mCricketDataList;

	public CJRLiveMatchesAdapter(Context mContext, List<CricketModel> mCricketDataList) {
		myInflater=LayoutInflater.from(mContext);
		this.mCricketDataList=mCricketDataList;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return mCricketDataList.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
	
		convertView=myInflater.inflate(R.layout.adapter_live_match, null);
		TextView title=(TextView)convertView.findViewById(R.id.Title);
		title.setText(Html.fromHtml(mCricketDataList.get(position).getTeams()));
		TextView teamAlist=(TextView)convertView.findViewById(R.id.teamA);
		teamAlist.setText(Html.fromHtml(mCricketDataList.get(position).getTeam1()));
		TextView teamBlist=(TextView)convertView.findViewById(R.id.teamB);
		teamBlist.setText(Html.fromHtml(mCricketDataList.get(position).getTeam2()));
		
	
		return convertView;
	}
}

