package com.criclive.criclive.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.criclive.criclive.R;

import java.util.List;


public class CJRTeamListing extends BaseAdapter {

    LayoutInflater myInflater;
    int noMatch = 0;
    List<String> title;
    Context parent;

    public CJRTeamListing(Context parent, List<String> titles) {
        myInflater = LayoutInflater.from(parent);
        this.title = titles;
        this.parent = parent;
        // TODO Auto-generated constructor stub
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return title.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if (convertView == null) {
            convertView = myInflater.inflate(R.layout.adapter_team_list, null);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.Title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.title.setText(title.get(position));
        return convertView;

    }

    class ViewHolder {
        TextView title;
    }
}

