package com.criclive.criclive.adapter;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.criclive.criclive.R;
import com.criclive.criclive.model.CricketModel;

import java.util.List;


public class CJRRecentUpcoming extends BaseAdapter {

    private LayoutInflater myInflater;
    private List<CricketModel> mListData;

    public CJRRecentUpcoming(Context mContext, List<CricketModel> mListData) {
        myInflater = LayoutInflater.from(mContext);
        this.mListData=mListData;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return mListData.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if (convertView == null) {
            convertView = myInflater.inflate(R.layout.adapter_recent_upcoming, null);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.txt_title);
            holder.description = (TextView) convertView.findViewById(R.id.txt_description);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Spanned title = Html.fromHtml(mListData.get(position).getTitle());
        String html = mListData.get(position).getDescription();
/*        html = html.replaceAll("<(.*?)\\>", " ");//Removes all items in brackets
        html = html.replaceAll("<(.*?)\\\n", " ");//Must be undeneath
        html = html.replaceFirst("(.*?)\\>", " ");//Removes any connected item to the last bracket
        html = html.replaceAll("&nbsp;", " ");
        html = html.replaceAll("&amp;", " ");*/
        Spanned des=Html.fromHtml(html);


        holder.title.setText(title.toString());
        holder.description.setText(des.toString());

        return convertView;

    }

    class ViewHolder {
        TextView title, description;
    }
}

