package com.criclive.criclive.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.criclive.criclive.R;
import com.criclive.criclive.model.CricketModel;
import com.squareup.picasso.Picasso;

import java.util.List;


public class CJRCricketNewsListing extends BaseAdapter {

    private LayoutInflater myInflater;
    private List<CricketModel> mNewsListData;
    private Context mContext;

    public CJRCricketNewsListing(Context mContext, List<CricketModel> mNewsListData) {
        this.mNewsListData = mNewsListData;
        myInflater = LayoutInflater.from(mContext);
        this.mContext = mContext;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return mNewsListData.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if (convertView == null) {
            convertView = myInflater.inflate(R.layout.adapter_news_list, null);
            holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtTitle.setText(Html.fromHtml(mNewsListData.get(position).getTitle()));
        if(!TextUtils.isEmpty(mNewsListData.get(position).getImage()))
        {
            Picasso.with(mContext)
                    .load(mNewsListData.get(position).getImage())
                    .error(R.drawable.defaultimage)
                    .placeholder(R.drawable.defaultimage)
                    .into(holder.imageView);
        }
        return convertView;

    }

    class ViewHolder {
        TextView txtTitle;
        ImageView imageView;
    }
}