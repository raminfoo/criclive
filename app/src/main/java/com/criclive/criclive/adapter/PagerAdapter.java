package com.criclive.criclive.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.criclive.criclive.fragment.LiveMatches;
import com.criclive.criclive.fragment.NewsList;
import com.criclive.criclive.fragment.RecentMatches;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
 
    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }
 
    @Override
    public Fragment getItem(int position) {
 
        switch (position) {
            case 0:
                LiveMatches liveFragment = new LiveMatches();
                return liveFragment;
            case 1:
                RecentMatches recentFragment = new RecentMatches();
                Bundle bundle=new Bundle();
                bundle.putString("Url", "http://feeds.feedburner.com/bank4study.xml");
                recentFragment.setArguments(bundle);
                return recentFragment;
            case 2:
                RecentMatches upcomingFragment = new RecentMatches();
                bundle=new Bundle();
                bundle.putString("Url", "http://cricket.worldsnap.com/cricket/u/m/30/0/International/rss.html");
                upcomingFragment.setArguments(bundle);
                return upcomingFragment;
            case 3:
                NewsList newsFragment = new NewsList();
                return newsFragment;
            default:
                return null;
        }
    }
 
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}