package com.criclive.criclive.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.criclive.criclive.R;
import com.criclive.criclive.utils.TeamUtils;

import java.util.List;

public class SpinnerAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private int size = 0;
    private List<String> mItems;

    public SpinnerAdapter(Context mContext, Boolean isInternational) {
        inflater = LayoutInflater.from(mContext);
        if (isInternational)
            mItems = TeamUtils.getInternationalList();
        size = mItems.size();
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return size;
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = inflater.inflate(R.layout.adapter_team_list, null);
        TextView title = (TextView) convertView.findViewById(R.id.Title);
        title.setText(Html.fromHtml(mItems.get(position)));
        return convertView;
    }
}

