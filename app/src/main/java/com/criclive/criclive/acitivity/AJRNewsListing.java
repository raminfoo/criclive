package com.criclive.criclive.acitivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.criclive.criclive.R;
import com.criclive.criclive.adapter.CJRCricketNewsListing;
import com.criclive.criclive.model.CricketModel;
import com.criclive.criclive.utils.AndroidUtils;
import com.criclive.criclive.utils.XMLParser;
import com.criclive.criclive.volley.CustomVolleyRequestQueue;

import java.util.List;

public class AJRNewsListing extends CustomActivity {

    private String mTeamId = "";
    private ListView mNewsList;
    private String feedUrlString = "", mSelectedTeam = "";
    private ProgressBar mProgressBar;
    private List<CricketModel> mNewsListData;
    private TextView mHeading;
    private Context mContext;
    private RequestQueue mQueue;
    private LinearLayout mNoInternetView;
    private TextView mNoMatch;
    private Button mRefresh;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_listing);

        mContext = getApplicationContext();
        initView();
        getIntentData();
        loadData();
    }

    private void initView() {
        mNewsList = (ListView) findViewById(R.id.newsList);
        mHeading = (TextView) findViewById(R.id.txt_news_heading);
        mRefresh = (Button)findViewById(R.id.btn_refresh);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressBar.setVisibility(View.VISIBLE);
                mNoInternetView.setVisibility(View.GONE);
                loadData();
            }
        });

        mNoInternetView = (LinearLayout)findViewById(R.id.noInternetView);
        mNoMatch = (TextView)findViewById(R.id.nomatch);
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        mTeamId = bundle.getString("selectedId");
        mSelectedTeam = bundle.getString("selectedTeam");

        if (mTeamId.equals("0")) {
            feedUrlString = "http://www.rediff.com/rss/cricketrss.xml";
            mHeading.setText("Latest News".toUpperCase());
        } else {
            feedUrlString = "http://www.espncricinfo.com/rss/content/feeds/news/" + mTeamId + ".xml";
            mHeading.setText(mSelectedTeam.toUpperCase());
        }
    }

    private void loadData() {
        if (AndroidUtils.isConnected(mContext)) {
            mQueue = CustomVolleyRequestQueue.getInstance(mContext).getRequestQueue();
            StringRequest req = new StringRequest(Request.Method.GET, feedUrlString,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            mNewsListData = XMLParser.parseData(response);
                            if (mNewsListData!=null&&mNewsListData.size()>0) {
                                updateUIwithData();
                            } else {
                                showErrorMessage("Please connect to the Internet to continue...");
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            showErrorMessage("Please connect to the Internet to continue...");
                        }
                    }
            );
            mQueue.add(req);
        } else {
            showErrorMessage("Please connect to the Internet to continue...");
        }
    }

    private void showErrorMessage(String errorMessage) {
        mProgressBar.setVisibility(View.GONE);
        mNewsList.setVisibility(View.GONE);
        mNoInternetView.setVisibility(View.VISIBLE);
        mNoMatch.setText(errorMessage);
    }

    public void updateUIwithData() {
        try {
            AnimationSet set = new AnimationSet(true);

            Animation animation = new AlphaAnimation(0.0f, 1.0f);
            animation.setDuration(400);
            set.addAnimation(animation);

            animation = new TranslateAnimation(
                    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
            );
            animation.setDuration(400);
            set.addAnimation(animation);

            LayoutAnimationController controller =
                    new LayoutAnimationController(set, 0.25f);

            mNewsList.setLayoutAnimation(controller);
            mNewsList.setAdapter(new CJRCricketNewsListing(mContext,mNewsListData));
            mNewsList.setOnItemClickListener(new OnItemClickListener() {

                public void onItemClick(AdapterView<?> arg0, View arg1,
                                        int position, long arg3) {
                    Intent intent = new Intent(mContext, AJRWebPage.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("url", mNewsListData.get(position).getLink());
                    bundle.putString("teamName", mNewsListData.get(position).getTitle());
                    intent.putExtras(bundle);
                    startActivityForResult(intent, 100);

                }
            });
            mProgressBar.setVisibility(View.GONE);
        } catch (Throwable t) {
            Log.e("AndroidNews", t.getMessage(), t);
        }
    }
}
