package com.criclive.criclive.acitivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.criclive.criclive.R;
import com.criclive.criclive.adapter.CJRRecentUpcoming;
import com.criclive.criclive.model.CricketModel;
import com.criclive.criclive.utils.AndroidUtils;
import com.criclive.criclive.utils.XMLParser;
import com.criclive.criclive.volley.CustomVolleyRequestQueue;

import java.util.List;

public class AJRScoreCard extends CustomActivity {

    private List<CricketModel> mListData;
    private ListView mListView;
    private String url = "http://live-feeds.cricbuzz.com/CricbuzzFeed";
    private Context mContext;
    private RequestQueue mQueue;
    private TextView noMatch;
    private ProgressBar mProgressBar;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_card);
        mContext=getApplicationContext();
        mListView = (ListView) findViewById(R.id.list);
        noMatch = (TextView) findViewById(R.id.nomatch);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);

        Button back = (Button) findViewById(R.id.btn_back);
        back.setOnClickListener(new OnClickListener() {


            public void onClick(View v) {
                finish();
            }
        });

        Button btn_refresh = (Button) findViewById(R.id.btn_refresh);
        btn_refresh.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mProgressBar.setVisibility(View.VISIBLE);
                loadData();
            }
        });

        loadData();
    }

    private void loadData() {
        if (AndroidUtils.isConnected(mContext)) {
            mQueue = CustomVolleyRequestQueue.getInstance(mContext).getRequestQueue();
            StringRequest req = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            mListData = XMLParser.parseData(response);
                            if (mListData!=null&&mListData.size()>0) {
                                updateUIwithData();
                            } else {
                                showErrorMessage("Please connect to the Internet to continue...");
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            showErrorMessage("Please connect to the Internet to continue...");
                        }
                    }
            );
            mQueue.add(req);
        } else {
            showErrorMessage("Please connect to the Internet to continue...");
        }
    }

    private void showErrorMessage(String message) {
        mListView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
        noMatch.setVisibility(View.VISIBLE);
        noMatch.setText(message);
    }


    public void updateUIwithData() {
        try {
            AnimationSet set = new AnimationSet(true);

            Animation animation = new AlphaAnimation(0.0f, 1.0f);
            animation.setDuration(400);
            set.addAnimation(animation);

            animation = new TranslateAnimation(
                    Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
            );
            animation.setDuration(400);
            set.addAnimation(animation);

            LayoutAnimationController controller =
                    new LayoutAnimationController(set, 0.25f);

            mListView.setLayoutAnimation(controller);
            mListView.setAdapter(new CJRRecentUpcoming(mContext, mListData));
            mListView.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
            noMatch.setVisibility(View.GONE);
        } catch (Throwable t) {
            Log.e("AndroidNews", t.getMessage(), t);
        }
    }
}
