package com.criclive.criclive.acitivity;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.criclive.criclive.R;

public class AJRSplashScreen extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_splashscreen);
		setProgressBarVisibility(true);

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent();
				intent.setClass(AJRSplashScreen.this, AJRHomePage.class);
				startActivity(intent);
				finish();
			}
		}, 2000);
	}

}
