package com.criclive.criclive.acitivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.criclive.criclive.R;


public class AJRWebPage extends Activity {

    private WebView mWebView;
    private String url = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webpage);

        mWebView = (WebView) findViewById(R.id.wlive_score);

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setFocusableInTouchMode(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle.containsKey("url"))
            url = bundle.getString("url");


        TextView txtTeam = (TextView) findViewById(R.id.txt_teams);
        txtTeam.setText(bundle.containsKey("teamName")?bundle.getString("teamName") :
                "http://www.espncricinfo.com/");


        Button back = (Button) findViewById(R.id.btn_back);
        back.setOnClickListener(new OnClickListener() {


            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });

        mWebView.setWebViewClient(new HelloWebViewClient());

        final ProgressBar loadingProgressBar = (ProgressBar) findViewById(R.id.progressbar_Horizontal);
        mWebView.setWebChromeClient(new WebChromeClient() {

            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                loadingProgressBar.setProgress(newProgress);
                if (newProgress == 100) {
                    loadingProgressBar.setVisibility(View.GONE);
                } else {
                    loadingProgressBar.setVisibility(View.VISIBLE);
                }
            }

        });

        mWebView.loadUrl(url.toString());
    }


    public void onPause() {

        super.onPause();

    }

    public void onResume() {

        super.onResume();

    }


    public void onDestroy() {

        super.onDestroy();
    }


    /**
     * Method to call Parser and cancel working timer Thread.
     */


    private class HelloWebViewClient extends WebViewClient {

        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            mWebView.setVisibility(View.INVISIBLE);
            AlertDialog.Builder alertbox = new AlertDialog.Builder(AJRWebPage.this);
            alertbox.setTitle("Error");
            alertbox.setIcon(android.R.drawable.ic_dialog_alert);
            alertbox.setMessage(description);
            alertbox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    arg0.dismiss();
                    finish();
                }
            });
            alertbox.show();
        }

    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }


    /**
     * CG Code to Handle Back IN WebView---and all back key
     */


    public boolean onKeyDown(int keyCode, KeyEvent event1) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event1);
    }

}